<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New M Factura'), ['action' => 'add']) ?></li>
    </ul>
</div>
<div class="mFacturas index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id_m_factura') ?></th>
            <th><?= $this->Paginator->sort('nro_factura') ?></th>
            <th><?= $this->Paginator->sort('kilo_prod') ?></th>
            <th><?= $this->Paginator->sort('costo_prod') ?></th>
            <th><?= $this->Paginator->sort('precio_venta') ?></th>
            <th><?= $this->Paginator->sort('costo_flete') ?></th>
            <th><?= $this->Paginator->sort('beneficio_marginal') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($mFacturas as $mFactura): ?>
        <tr>
            <td><?= $this->Number->format($mFactura->id_m_factura) ?></td>
            <td><?= h($mFactura->nro_factura) ?></td>
            <td><?= $this->Number->format($mFactura->kilo_prod) ?></td>
            <td><?= $this->Number->format($mFactura->costo_prod) ?></td>
            <td><?= $this->Number->format($mFactura->precio_venta) ?></td>
            <td><?= $this->Number->format($mFactura->costo_flete) ?></td>
            <td><?= $this->Number->format($mFactura->beneficio_marginal) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $mFactura->id_m_factura]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $mFactura->id_m_factura]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $mFactura->id_m_factura], ['confirm' => __('Are you sure you want to delete # {0}?', $mFactura->id_m_factura)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
