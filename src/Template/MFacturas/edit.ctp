<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $mFactura->id_m_factura],
                ['confirm' => __('Are you sure you want to delete # {0}?', $mFactura->id_m_factura)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List M Facturas'), ['action' => 'index']) ?></li>
    </ul>
</div>
<div class="mFacturas form large-10 medium-9 columns">
    <?= $this->Form->create($mFactura) ?>
    <fieldset>
        <legend><?= __('Edit M Factura') ?></legend>
        <?php
            echo $this->Form->input('nro_factura');
            echo $this->Form->input('kilo_prod');
            echo $this->Form->input('costo_prod');
            echo $this->Form->input('precio_venta');
            echo $this->Form->input('costo_flete');
            echo $this->Form->input('beneficio_marginal');
            echo $this->Form->input('beneficio_total');
            echo $this->Form->input('id_m_empresas');
            echo $this->Form->input('m_fact_fecha');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
