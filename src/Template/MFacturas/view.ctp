<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit M Factura'), ['action' => 'edit', $mFactura->id_m_factura]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete M Factura'), ['action' => 'delete', $mFactura->id_m_factura], ['confirm' => __('Are you sure you want to delete # {0}?', $mFactura->id_m_factura)]) ?> </li>
        <li><?= $this->Html->link(__('List M Facturas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New M Factura'), ['action' => 'add']) ?> </li>
    </ul>
</div>
<div class="mFacturas view large-10 medium-9 columns">
    <h2><?= h($mFactura->id_m_factura) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Nro Factura') ?></h6>
            <p><?= h($mFactura->nro_factura) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id M Factura') ?></h6>
            <p><?= $this->Number->format($mFactura->id_m_factura) ?></p>
            <h6 class="subheader"><?= __('Kilo Prod') ?></h6>
            <p><?= $this->Number->format($mFactura->kilo_prod) ?></p>
            <h6 class="subheader"><?= __('Costo Prod') ?></h6>
            <p><?= $this->Number->format($mFactura->costo_prod) ?></p>
            <h6 class="subheader"><?= __('Precio Venta') ?></h6>
            <p><?= $this->Number->format($mFactura->precio_venta) ?></p>
            <h6 class="subheader"><?= __('Costo Flete') ?></h6>
            <p><?= $this->Number->format($mFactura->costo_flete) ?></p>
            <h6 class="subheader"><?= __('Beneficio Marginal') ?></h6>
            <p><?= $this->Number->format($mFactura->beneficio_marginal) ?></p>
            <h6 class="subheader"><?= __('Beneficio Total') ?></h6>
            <p><?= $this->Number->format($mFactura->beneficio_total) ?></p>
            <h6 class="subheader"><?= __('Id M Empresas') ?></h6>
            <p><?= $this->Number->format($mFactura->id_m_empresas) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('M Fact Fecha') ?></h6>
            <p><?= h($mFactura->m_fact_fecha) ?></p>
        </div>
    </div>
</div>
