<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $mPago->id_m_pagos],
                ['confirm' => __('Are you sure you want to delete # {0}?', $mPago->id_m_pagos)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List M Pagos'), ['action' => 'index']) ?></li>
    </ul>
</div>
<div class="mPagos form large-10 medium-9 columns">
    <?= $this->Form->create($mPago) ?>
    <fieldset>
        <legend><?= __('Edit M Pago') ?></legend>
        <?php
            echo $this->Form->input('id_m_empresas');
            echo $this->Form->input('kilo_reciv');
            echo $this->Form->input('costo_mat_prim');
            echo $this->Form->input('m_pagos_fact');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
