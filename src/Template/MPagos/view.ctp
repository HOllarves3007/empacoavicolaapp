<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit M Pago'), ['action' => 'edit', $mPago->id_m_pagos]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete M Pago'), ['action' => 'delete', $mPago->id_m_pagos], ['confirm' => __('Are you sure you want to delete # {0}?', $mPago->id_m_pagos)]) ?> </li>
        <li><?= $this->Html->link(__('List M Pagos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New M Pago'), ['action' => 'add']) ?> </li>
    </ul>
</div>
<div class="mPagos view large-10 medium-9 columns">
    <h2><?= h($mPago->id_m_pagos) ?></h2>
    <div class="row">
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id M Pagos') ?></h6>
            <p><?= $this->Number->format($mPago->id_m_pagos) ?></p>
            <h6 class="subheader"><?= __('Id M Empresas') ?></h6>
            <p><?= $this->Number->format($mPago->id_m_empresas) ?></p>
            <h6 class="subheader"><?= __('Kilo Reciv') ?></h6>
            <p><?= $this->Number->format($mPago->kilo_reciv) ?></p>
            <h6 class="subheader"><?= __('Costo Mat Prim') ?></h6>
            <p><?= $this->Number->format($mPago->costo_mat_prim) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('M Pagos Fact') ?></h6>
            <p><?= h($mPago->m_pagos_fact) ?></p>
        </div>
    </div>
</div>
