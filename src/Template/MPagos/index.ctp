<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New M Pago'), ['action' => 'add']) ?></li>
    </ul>
</div>
<div class="mPagos index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id_m_pagos') ?></th>
            <th><?= $this->Paginator->sort('id_m_empresas') ?></th>
            <th><?= $this->Paginator->sort('kilo_reciv') ?></th>
            <th><?= $this->Paginator->sort('costo_mat_prim') ?></th>
            <th><?= $this->Paginator->sort('m_pagos_fact') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($mPagos as $mPago): ?>
        <tr>
            <td><?= $this->Number->format($mPago->id_m_pagos) ?></td>
            <td><?= $this->Number->format($mPago->id_m_empresas) ?></td>
            <td><?= $this->Number->format($mPago->kilo_reciv) ?></td>
            <td><?= $this->Number->format($mPago->costo_mat_prim) ?></td>
            <td><?= h($mPago->m_pagos_fact) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $mPago->id_m_pagos]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $mPago->id_m_pagos]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $mPago->id_m_pagos], ['confirm' => __('Are you sure you want to delete # {0}?', $mPago->id_m_pagos)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
