<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit M Empresa'), ['action' => 'edit', $mEmpresa->id_m_empresas]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete M Empresa'), ['action' => 'delete', $mEmpresa->id_m_empresas], ['confirm' => __('Are you sure you want to delete # {0}?', $mEmpresa->id_m_empresas)]) ?> </li>
        <li><?= $this->Html->link(__('List M Empresas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New M Empresa'), ['action' => 'add']) ?> </li>
    </ul>
</div>
<div class="mEmpresas view large-10 medium-9 columns">
    <h2><?= h($mEmpresa->id_m_empresas) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Emp Nom') ?></h6>
            <p><?= h($mEmpresa->emp_nom) ?></p>
            <h6 class="subheader"><?= __('Emp Dir') ?></h6>
            <p><?= h($mEmpresa->emp_dir) ?></p>
            <h6 class="subheader"><?= __('Emp Num') ?></h6>
            <p><?= h($mEmpresa->emp_num) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id M Empresas') ?></h6>
            <p><?= $this->Number->format($mEmpresa->id_m_empresas) ?></p>
        </div>
    </div>
</div>
