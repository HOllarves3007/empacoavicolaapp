<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $mEmpresa->id_m_empresas],
                ['confirm' => __('Are you sure you want to delete # {0}?', $mEmpresa->id_m_empresas)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List M Empresas'), ['action' => 'index']) ?></li>
    </ul>
</div>
<div class="mEmpresas form large-10 medium-9 columns">
    <?= $this->Form->create($mEmpresa) ?>
    <fieldset>
        <legend><?= __('Edit M Empresa') ?></legend>
        <?php
            echo $this->Form->input('emp_nom');
            echo $this->Form->input('emp_dir');
            echo $this->Form->input('emp_num');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
