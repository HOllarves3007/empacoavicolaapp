<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New M Empresa'), ['action' => 'add']) ?></li>
    </ul>
</div>
<div class="mEmpresas index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id_m_empresas') ?></th>
            <th><?= $this->Paginator->sort('emp_nom') ?></th>
            <th><?= $this->Paginator->sort('emp_dir') ?></th>
            <th><?= $this->Paginator->sort('emp_num') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($mEmpresas as $mEmpresa): ?>
        <tr>
            <td><?= $this->Number->format($mEmpresa->id_m_empresas) ?></td>
            <td><?= h($mEmpresa->emp_nom) ?></td>
            <td><?= h($mEmpresa->emp_dir) ?></td>
            <td><?= h($mEmpresa->emp_num) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $mEmpresa->id_m_empresas]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $mEmpresa->id_m_empresas]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $mEmpresa->id_m_empresas], ['confirm' => __('Are you sure you want to delete # {0}?', $mEmpresa->id_m_empresas)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
