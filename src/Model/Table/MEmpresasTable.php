<?php
namespace App\Model\Table;

use App\Model\Entity\MEmpresa;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MEmpresas Model
 *
 */
class MEmpresasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('m_empresas');
        $this->displayField('id_m_empresas');
        $this->primaryKey('id_m_empresas');

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id_m_empresas', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id_m_empresas', 'create');

        $validator
            ->requirePresence('emp_nom', 'create')
            ->notEmpty('emp_nom');

        $validator
            ->requirePresence('emp_dir', 'create')
            ->notEmpty('emp_dir');

        $validator
            ->allowEmpty('emp_num');

        return $validator;
    }
}
