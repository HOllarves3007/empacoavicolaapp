<?php
namespace App\Model\Table;

use App\Model\Entity\MPago;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MPagos Model
 *
 */
class MPagosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('m_pagos');
        $this->displayField('id_m_pagos');
        $this->primaryKey('id_m_pagos');

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id_m_pagos', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id_m_pagos', 'create');

        $validator
            ->add('id_m_empresas', 'valid', ['rule' => 'numeric'])
            ->requirePresence('id_m_empresas', 'create')
            ->notEmpty('id_m_empresas');

        $validator
            ->add('kilo_reciv', 'valid', ['rule' => 'numeric'])
            ->requirePresence('kilo_reciv', 'create')
            ->notEmpty('kilo_reciv');

        $validator
            ->add('costo_mat_prim', 'valid', ['rule' => 'numeric'])
            ->requirePresence('costo_mat_prim', 'create')
            ->notEmpty('costo_mat_prim');

        $validator
            ->add('m_pagos_fact', 'valid', ['rule' => 'date'])
            ->requirePresence('m_pagos_fact', 'create')
            ->notEmpty('m_pagos_fact');

        return $validator;
    }
}
