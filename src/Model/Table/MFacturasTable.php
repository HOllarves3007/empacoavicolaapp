<?php
namespace App\Model\Table;

use App\Model\Entity\MFactura;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MFacturas Model
 *
 */
class MFacturasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('m_facturas');
        $this->displayField('id_m_factura');
        $this->primaryKey('id_m_factura');

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id_m_factura', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id_m_factura', 'create');

        $validator
            ->requirePresence('nro_factura', 'create')
            ->notEmpty('nro_factura');

        $validator
            ->add('kilo_prod', 'valid', ['rule' => 'numeric'])
            ->requirePresence('kilo_prod', 'create')
            ->notEmpty('kilo_prod');

        $validator
            ->add('costo_prod', 'valid', ['rule' => 'numeric'])
            ->requirePresence('costo_prod', 'create')
            ->notEmpty('costo_prod');

        $validator
            ->add('precio_venta', 'valid', ['rule' => 'numeric'])
            ->requirePresence('precio_venta', 'create')
            ->notEmpty('precio_venta');

        $validator
            ->add('costo_flete', 'valid', ['rule' => 'numeric'])
            ->requirePresence('costo_flete', 'create')
            ->notEmpty('costo_flete');

        $validator
            ->add('beneficio_marginal', 'valid', ['rule' => 'numeric'])
            ->requirePresence('beneficio_marginal', 'create')
            ->notEmpty('beneficio_marginal');

        $validator
            ->add('beneficio_total', 'valid', ['rule' => 'numeric'])
            ->requirePresence('beneficio_total', 'create')
            ->notEmpty('beneficio_total');

        $validator
            ->add('id_m_empresas', 'valid', ['rule' => 'numeric'])
            ->requirePresence('id_m_empresas', 'create')
            ->notEmpty('id_m_empresas');

        $validator
            ->add('m_fact_fecha', 'valid', ['rule' => 'date'])
            ->requirePresence('m_fact_fecha', 'create')
            ->notEmpty('m_fact_fecha');

        return $validator;
    }
}
