<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MPagos Controller
 *
 * @property \App\Model\Table\MPagosTable $MPagos
 */
class MPagosController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('mPagos', $this->paginate($this->MPagos));
        $this->set('_serialize', ['mPagos']);
    }

    /**
     * View method
     *
     * @param string|null $id M Pago id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $mPago = $this->MPagos->get($id, [
            'contain' => []
        ]);
        $this->set('mPago', $mPago);
        $this->set('_serialize', ['mPago']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $mPago = $this->MPagos->newEntity();
        if ($this->request->is('post')) {
            $mPago = $this->MPagos->patchEntity($mPago, $this->request->data);
            if ($this->MPagos->save($mPago)) {
                $this->Flash->success(__('The m pago has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The m pago could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('mPago'));
        $this->set('_serialize', ['mPago']);
    }

    /**
     * Edit method
     *
     * @param string|null $id M Pago id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $mPago = $this->MPagos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $mPago = $this->MPagos->patchEntity($mPago, $this->request->data);
            if ($this->MPagos->save($mPago)) {
                $this->Flash->success(__('The m pago has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The m pago could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('mPago'));
        $this->set('_serialize', ['mPago']);
    }

    /**
     * Delete method
     *
     * @param string|null $id M Pago id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $mPago = $this->MPagos->get($id);
        if ($this->MPagos->delete($mPago)) {
            $this->Flash->success(__('The m pago has been deleted.'));
        } else {
            $this->Flash->error(__('The m pago could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
