<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MEmpresas Controller
 *
 * @property \App\Model\Table\MEmpresasTable $MEmpresas
 */
class MEmpresasController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('mEmpresas', $this->paginate($this->MEmpresas));
        $this->set('_serialize', ['mEmpresas']);
    }

    /**
     * View method
     *
     * @param string|null $id M Empresa id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $mEmpresa = $this->MEmpresas->get($id, [
            'contain' => []
        ]);
        $this->set('mEmpresa', $mEmpresa);
        $this->set('_serialize', ['mEmpresa']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $mEmpresa = $this->MEmpresas->newEntity();
        if ($this->request->is('post')) {
            $mEmpresa = $this->MEmpresas->patchEntity($mEmpresa, $this->request->data);
            if ($this->MEmpresas->save($mEmpresa)) {
                $this->Flash->success(__('The m empresa has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The m empresa could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('mEmpresa'));
        $this->set('_serialize', ['mEmpresa']);
    }

    /**
     * Edit method
     *
     * @param string|null $id M Empresa id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $mEmpresa = $this->MEmpresas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $mEmpresa = $this->MEmpresas->patchEntity($mEmpresa, $this->request->data);
            if ($this->MEmpresas->save($mEmpresa)) {
                $this->Flash->success(__('The m empresa has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The m empresa could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('mEmpresa'));
        $this->set('_serialize', ['mEmpresa']);
    }

    /**
     * Delete method
     *
     * @param string|null $id M Empresa id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $mEmpresa = $this->MEmpresas->get($id);
        if ($this->MEmpresas->delete($mEmpresa)) {
            $this->Flash->success(__('The m empresa has been deleted.'));
        } else {
            $this->Flash->error(__('The m empresa could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
