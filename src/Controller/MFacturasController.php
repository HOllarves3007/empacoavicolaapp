<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MFacturas Controller
 *
 * @property \App\Model\Table\MFacturasTable $MFacturas
 */
class MFacturasController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('mFacturas', $this->paginate($this->MFacturas));
        $this->set('_serialize', ['mFacturas']);
    }

    /**
     * View method
     *
     * @param string|null $id M Factura id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $mFactura = $this->MFacturas->get($id, [
            'contain' => []
        ]);
        $this->set('mFactura', $mFactura);
        $this->set('_serialize', ['mFactura']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $mFactura = $this->MFacturas->newEntity();
        if ($this->request->is('post')) {
            $mFactura = $this->MFacturas->patchEntity($mFactura, $this->request->data);
            if ($this->MFacturas->save($mFactura)) {
                $this->Flash->success(__('The m factura has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The m factura could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('mFactura'));
        $this->set('_serialize', ['mFactura']);
    }

    /**
     * Edit method
     *
     * @param string|null $id M Factura id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $mFactura = $this->MFacturas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $mFactura = $this->MFacturas->patchEntity($mFactura, $this->request->data);
            if ($this->MFacturas->save($mFactura)) {
                $this->Flash->success(__('The m factura has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The m factura could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('mFactura'));
        $this->set('_serialize', ['mFactura']);
    }

    /**
     * Delete method
     *
     * @param string|null $id M Factura id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $mFactura = $this->MFacturas->get($id);
        if ($this->MFacturas->delete($mFactura)) {
            $this->Flash->success(__('The m factura has been deleted.'));
        } else {
            $this->Flash->error(__('The m factura could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
